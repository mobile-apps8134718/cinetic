import {Theme} from '@react-navigation/native';

export const LightTheme: Theme = {
  dark: false,
  colors: {
    primary: '#fff',
    background: '#fff',
    card: '#fff',
    text: '#fff',
    border: '#fff',
    notification: '#fff',
  },
};

export const DarkTheme: Theme = {
  dark: true,
  colors: {
    primary: '#000000',
    background: '#000000',
    card: '#000000',
    text: 'white',
    border: '#000000',
    notification: '#000000',
  },
};
