/**
 * Basic Colors
 */

export const colors = {
  black: '#000000',
  red: '#FF0000',
  green: '#00FF00',
  blue: '#0000FF',
  orange: '#FFA500',
  purple: '#800080',
  gray: '#808080',

  backgroundPurple: '#1A2435',
  lightBackground: '#1E273A',
  fontColor: '#637394',
  borderColor: '#233049',
  yellow: '#FE782A',

  white: '#FFFFFF',

  errorRed: '#AA4A44',
};

export default colors;
