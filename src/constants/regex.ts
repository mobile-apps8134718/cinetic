/**
 * Regular expressions for mobile banking entities
 */

/**
 * Email Address
 *
 * @description Alphanumeric characters, dots, underscores, hyphens allowed.
 * @validationRules Valid email format.
 */
export const emailRegex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

/**
 * Password
 *
 * @description Minimum 8 characters with at least one uppercase letter, one lowercase letter, and one digit.
 * @validationRules Alphanumeric and special characters allowed.
 */
export const passwordRegex =
  /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d!@#$%^&*()\-_=+{}[\]|;:'",.<>/?`~]{8,}$/;

/**
 * Username
 *
 * @description Alphanumeric characters, dots, underscores, hyphens allowed.
 * @validationRules 3-20 characters.
 */
export const usernameRegex = /^[a-zA-Z0-9._-]{3,20}$/;

/**
 * Phone Number
 *
 * @description Numeric digits only.
 * @validationRules 10-15 digits.
 */
export const phoneNumberRegex = /^\d{10,15}$/;

/**
 * Date of Birth (DD/MM/YYYY format)
 *
 * @description Numeric digits separated by forward slashes.
 * @validationRules Valid date format.
 */
export const dobRegex = /^\d{2}\/\d{2}\/\d{4}$/;

/**
 * Account Number
 *
 * @description Alphanumeric characters allowed.
 * @validationRules 8-12 characters.
 */
export const accountNumberRegex = /^[a-zA-Z0-9]{8,12}$/;

/**
 * Credit Card Number
 *
 * @description Numeric digits only.
 * @validationRules 16 digits.
 */
export const creditCardNumberRegex = /^\d{16}$/;

/**
 * Bank Routing Number
 *
 * @description Numeric digits only.
 * @validationRules 9 digits.
 */
export const routingNumberRegex = /^\d{9}$/;

/**
 * Social Security Number (SSN)
 *
 * @description Numeric digits only.
 * @validationRules 9 digits.
 */
export const ssnRegex = /^\d{9}$/;

/**
 * ZIP Code
 *
 * @description Numeric digits or alphanumeric characters allowed.
 * @validationRules 5 or 9 characters.
 */
export const zipCodeRegex = /^\d{5}(?:-\d{4})?$/;
