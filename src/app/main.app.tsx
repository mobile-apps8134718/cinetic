import {QueryClient, QueryClientProvider} from '@tanstack/react-query';
import {GestureHandlerRootView} from 'react-native-gesture-handler';
import {InitRefComponents} from 'app/initRefComponents';
import {enableScreens} from 'react-native-screens';
import {initTranslation} from 'lang/i18NextInit';
import {LogBox, View} from 'react-native';
import {appStyles} from 'app/appStyles';
import React from 'react';

initTranslation().then(() => {});
LogBox.ignoreAllLogs();
enableScreens();

export const MainApp = () => {
  return (
    <QueryClientProvider client={new QueryClient()}>
      <GestureHandlerRootView style={appStyles.appWrapper}>
        <View style={appStyles.innerWrapper}>
          <InitRefComponents />
        </View>
      </GestureHandlerRootView>
    </QueryClientProvider>
  );
};
