import {loaderGlobalRef} from '../components/global/loader/loader.ref';
import Loader from '../components/global/loader/loader';
import {RootRouter} from '../navigation/rootRouter';
import React from 'react';
export const InitRefComponents = () => {
  return (
    <>
      <RootRouter />
      <Loader ref={loaderGlobalRef} />
    </>
  );
};
