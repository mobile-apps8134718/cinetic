import {StyleSheet} from 'react-native';

export const appStyles = StyleSheet.create({
  appWrapper: {
    flex: 1,
  },
  innerWrapper: {
    width: '100%',
    height: '100%',
  },
});
