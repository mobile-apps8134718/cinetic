import {ActivityIndicator, View} from 'react-native';
import {loaderStyles} from '../globalComponentStyles';
import React, {PureComponent} from 'react';

export default class Loader extends PureComponent {
  state = {
    loaderIsActive: false,
  };

  start = () => {
    this.setState({loaderIsActive: true});
  };

  stop = () => {
    this.setState({loaderIsActive: false});
  };

  render() {
    return (
      this.state.loaderIsActive && (
        <View style={loaderStyles.absolute}>
          <ActivityIndicator
            color="red"
            size="large"
            shouldRasterizeIOS
            animating
          />
        </View>
      )
    );
  }
}
