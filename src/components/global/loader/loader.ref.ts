import {references} from 'components/componentReferences';
import Loader from './loader';

/**
  @param loaderRef accepts Loader component entirely
  @description use only for initialization at MainApp.tsx
 */
export const loaderGlobalRef = (loaderRef: Loader) => {
  references.loader = loaderRef;
};

/**
 @description Mounts the loader at the scale 100% width and height
 */
export const showLoader = () => {
  references.loader?.start();
};

/**
 @description Unmounts the loader
 */
export const hideLoader = () => {
  references.loader?.stop();
};
