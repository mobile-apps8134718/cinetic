import {StyleSheet} from 'react-native';

export const loaderStyles = StyleSheet.create({
  absolute: {
    position: 'absolute',
    top: '5%',
    left: 0,
    right: 0,
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
