import {InputWrapperProps} from '../sharedComponentInterfaces';
import {KeyboardAvoidingView, Platform} from 'react-native';
import {InputWrapperStyles} from '../sharedComponentStyles';
import React, {FC} from 'react';

export const InputWrapper: FC<InputWrapperProps> = ({children}) => {
  return (
    <KeyboardAvoidingView
      style={InputWrapperStyles.wrapper}
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      {children}
    </KeyboardAvoidingView>
  );
};
