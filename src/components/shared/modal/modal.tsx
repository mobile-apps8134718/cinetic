import BottomSheet, {BottomSheetProps} from '@gorhom/bottom-sheet';
import React, {FC} from 'react';

export const IModal: FC<BottomSheetProps> = props => {
  return <BottomSheet {...props}>{props.children}</BottomSheet>;
};
