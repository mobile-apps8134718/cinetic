import {State, TapGestureHandler} from 'react-native-gesture-handler';
import {MultiPressProps} from '../sharedComponentInterfaces';
import {FC, useRef} from 'react';
import {View} from 'react-native';
import React from 'react';

export const MultiPress: FC<MultiPressProps> = ({
  children = [],
  numberOfPress = 2,
  onDoublePress = () => {},
}) => {
  const doubleTapRef = useRef<TapGestureHandler>(null);

  const onMultiplePress = (event: {nativeEvent: {state: State}}) => {
    if (event.nativeEvent.state === State.ACTIVE) {
      onDoublePress();
    }
  };

  return (
    <TapGestureHandler waitFor={doubleTapRef}>
      <TapGestureHandler
        onHandlerStateChange={onMultiplePress}
        numberOfTaps={numberOfPress}
        ref={doubleTapRef}>
        <View>{children}</View>
      </TapGestureHandler>
    </TapGestureHandler>
  );
};
