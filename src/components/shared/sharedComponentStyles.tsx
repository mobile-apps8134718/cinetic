import {width} from '../../constants/dimensions';
import colors from '../../constants/colors';
import {StyleSheet} from 'react-native';

export const IButtonStyles = StyleSheet.create({
  wrapper: {
    width: width * 0.9,
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.yellow,
    borderRadius: 5,
    marginVertical: 10,
  },
  innerButtonWrapper: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold',
  },
  textVariant: {
    color: colors.fontColor,
    fontSize: 15,
    fontWeight: 'bold',
  },
});

export const CountryModalStyles = StyleSheet.create({
  handleIndicator: {
    backgroundColor: colors.white,
  },
  container: {
    zIndex: 1,
  },
  background: {
    backgroundColor: colors.backgroundPurple,
  },
  input: {
    marginBottom: 20,
  },
  bottomSheetList: {
    backgroundColor: colors.backgroundPurple,
  },
  listItemContainer: {
    width: '95%',
    alignSelf: 'center',
    borderRadius: 5,
    borderBottomColor: colors.fontColor,
    borderBottomWidth: 0.5,
    height: 50,
    marginVertical: 10,
    paddingHorizontal: '3%',
  },
  listItemContent: {
    width: '100%',
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconContainer: {
    width: 30,
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tickIcon: {
    marginRight: '5%',
  },
  listItemText: {
    color: colors.white,
  },
});

export const IInputStyles = StyleSheet.create({
  wrapper: {
    width: '95%',
    height: 75,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    marginVertical: 0,
    borderRadius: 5,
    overflow: 'hidden',
  },
  input: {
    width: '100%',
    height: '75%',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: colors.borderColor,
    marginVertical: 10,
    borderRadius: 5,
    fontSize: 16,
    color: colors.fontColor,
    paddingHorizontal: 15,
  },
  errInput: {
    width: '100%',
    height: '75%',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: colors.errorRed,
    marginVertical: 10,
    borderRadius: 5,
    fontSize: 16,
    color: colors.fontColor,
    paddingHorizontal: 15,
  },
  errorTextWrapper: {
    paddingHorizontal: 10,
    position: 'absolute',
    top: '0%',
    zIndex: 1,
    textAlign: 'center',
    right: '5%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.backgroundPurple,
  },
  text: {
    fontSize: 12,
    color: colors.errorRed,
  },
});

export const InputWrapperStyles = StyleSheet.create({
  wrapper: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export const TimerStyles = StyleSheet.create({
  resendTimerWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '3%',
  },
  labelText: {
    fontSize: 17,
    color: colors.fontColor,
    fontWeight: '500',
  },
});
