import React, {FC, ReactNode} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  TouchableOpacityProps,
} from 'react-native';

interface Props extends TouchableOpacityProps {
  children: ReactNode | ReactNode[];
}

export const SVGIconWrapper: FC<Props> = props => {
  return (
    <TouchableOpacity
      {...props}
      style={[styles.center, props.style]}
      hitSlop={{left: 10, right: 10, top: 10, bottom: 10}}>
      {props.children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  center: {
    width: '5%',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
