import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import {IInputProps} from '../sharedComponentInterfaces';
import {IInputStyles} from '../sharedComponentStyles';
import {Text, TextInput} from 'react-native';
import colors from 'constants/colors';
import React, {FC} from 'react';

export const Input: FC<IInputProps> = props => {
  const inputWrapperScale = useSharedValue(1);

  const {error = '', animationEnabled = true} = props;

  const wrapperAnimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{scale: withTiming(inputWrapperScale.value)}],
    };
  });

  const onFocus = () => {
    if (animationEnabled) {
      inputWrapperScale.value = 1.05;
    }
  };

  const onBlur = () => {
    if (animationEnabled) {
      inputWrapperScale.value = 1;
    }
  };

  return (
    <Animated.View style={[wrapperAnimatedStyle, IInputStyles.wrapper]}>
      {error !== '' && error !== undefined && error !== null && (
        <Animated.View style={IInputStyles.errorTextWrapper}>
          {error.length < 50 ? (
            <Text style={IInputStyles.text}>{error}</Text>
          ) : (
            <Text style={IInputStyles.text}>{error.slice(0, 45)}</Text>
          )}
        </Animated.View>
      )}

      <TextInput
        {...props}
        autoCapitalize="none"
        autoCorrect={false}
        autoFocus={false}
        maxLength={65}
        onBlur={onBlur}
        onFocus={onFocus}
        numberOfLines={1}
        style={[
          error ? IInputStyles.errInput : IInputStyles.input,
          props?.style,
        ]}
        placeholderTextColor={colors.fontColor}
      />
    </Animated.View>
  );
};
