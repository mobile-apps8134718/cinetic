import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import {TouchableOpacity, Text, ActivityIndicator} from 'react-native';
import {IButtonProps} from '../sharedComponentInterfaces';
import {IButtonStyles} from '../sharedComponentStyles';
import colors from 'constants/colors';
import React, {FC} from 'react';
import {throttle} from 'lodash';

export const Button: FC<IButtonProps> = ({
  title = '',
  textStyles = {},
  variant = 'Filled',
  loading = false,
  disabled = false,
  buttonStyles = {},
  throttleTime = 0,
  testID = '',
  onPress = () => {},
  onLongPress = () => {},
}) => {
  const scale = useSharedValue(1);

  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [{scale: withTiming(scale.value)}],
    };
  });

  const onThrottleLongPress = throttle(() => {
    onLongPress();
  }, throttleTime);

  const onThrottlePressIn = () => {
    scale.value = 0.9;
  };

  const onThrottlePressOut = () => {
    scale.value = 1;
    onPress();
  };

  return (
    <>
      {variant === 'Filled' ? (
        <Animated.View
          testID={testID}
          style={[animatedStyle, IButtonStyles.wrapper, buttonStyles]}>
          <TouchableOpacity
            hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
            style={IButtonStyles.innerButtonWrapper}
            onLongPress={onThrottleLongPress}
            onPressOut={onThrottlePressOut}
            onPressIn={onThrottlePressIn}
            disabled={disabled}
            activeOpacity={0.7}>
            {loading ? (
              <ActivityIndicator color={colors.backgroundPurple} size="small" />
            ) : (
              <Text style={[IButtonStyles.text, textStyles]}>{title}</Text>
            )}
          </TouchableOpacity>
        </Animated.View>
      ) : (
        <TouchableOpacity
          testID={testID}
          onPress={onPress}
          activeOpacity={0.7}
          style={buttonStyles}>
          <Text style={[IButtonStyles.textVariant, textStyles]}>{title}</Text>
        </TouchableOpacity>
      )}
    </>
  );
};
