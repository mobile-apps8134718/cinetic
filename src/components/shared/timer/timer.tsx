import {TimerProps} from '../sharedComponentInterfaces';
import React, {FC, useEffect, useState} from 'react';
import {Button} from '../button/button';
import {Text, TouchableOpacity} from 'react-native';
import {TimerStyles} from '../sharedComponentStyles';

export const Timer: FC<TimerProps> = ({
  label = '',
  count = 60,
  onPress = () => {},
  onLongPress = () => {},
}) => {
  const [seconds, setSeconds] = useState(count);

  useEffect(() => {
    const interval = setInterval(() => {
      setSeconds(prevSeconds => {
        if (prevSeconds === 0) {
          clearInterval(interval);
          return 0;
        }
        return prevSeconds - 1;
      });
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, [count, seconds, onPress]);

  const handleRestart = () => {
    setSeconds(count);
    onPress();
  };

  const onTimerPress = () => {
    handleRestart();
    onPress();
  };

  return (
    <TouchableOpacity
      onLongPress={onLongPress}
      onPress={onTimerPress}
      style={TimerStyles.resendTimerWrapper}>
      <Text style={TimerStyles.labelText}>{label}</Text>
      <Button
        testID={'TimerResetButton'}
        title={`${seconds}`}
        onPress={onTimerPress}
        variant="Text"
      />
    </TouchableOpacity>
  );
};
