import {StyleProp, TextInputProps, TextStyle, ViewStyle} from 'react-native';
import React, {ReactNode} from 'react';

export interface IButtonProps {
  title: string;
  testID: string;
  loading?: boolean;
  disabled?: boolean;
  throttleTime?: number;
  variant: 'Filled' | 'Text';
  onPress?: () => void;
  onLongPress?: () => void;
  buttonStyles?: StyleProp<ViewStyle>;
  textStyles?: StyleProp<TextStyle>;
}

export interface CountryData {
  id: string;
  country: string;
}

export interface CountryItemProps {
  item: CountryData;
  selected: string;
  onPress: (item: CountryData) => void;
}

export interface IInputProps extends TextInputProps {
  error?: string;
  animationEnabled?: boolean;
}

export interface InputWrapperProps {
  children: ReactNode | ReactNode[];
}

export interface MultiPressProps {
  numberOfPress: 2 | 3 | 4 | 5;
  onDoublePress: () => void;
  children: React.ReactNode | React.ReactNode[];
}

export interface TimerProps {
  label?: string;
  count?: number;
  onPress?: () => void;
  onLongPress?: () => void;
}

export interface CountryModalProps {
  show: boolean;
}
