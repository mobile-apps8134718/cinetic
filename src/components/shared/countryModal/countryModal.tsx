import {
  CountryData,
  CountryItemProps,
  CountryModalProps,
} from '../sharedComponentInterfaces';
import {CountryModalStyles} from '../sharedComponentStyles';
import {Text, TouchableOpacity, View} from 'react-native';
import {BottomSheetFlatList} from '@gorhom/bottom-sheet';
import React, {FC, useMemo, useState} from 'react';
import Tick from 'assets/icons/cross/tick.svg';
import {IModal} from '../modal/modal';
import {Input} from '../input/input';
import {intl} from '../../../hooks/intl';

const countryData: CountryData[] = [
  {id: '1', country: 'Afghanistan'},
  {id: '2', country: 'Albania'},
  {id: '3', country: 'Algeria'},
  {id: '4', country: 'Andorra'},
  {id: '5', country: 'Angola'},
  {id: '6', country: 'Argentina'},
  {id: '7', country: 'Armenia'},
  {id: '8', country: 'Australia'},
  {id: '9', country: 'Austria'},
  {id: '10', country: 'Bangladesh'},
  {id: '11', country: 'Belgium'},
  {id: '12', country: 'Brazil'},
  {id: '13', country: 'Bulgaria'},
];

const ListItem: FC<CountryItemProps> = ({item, selected, onPress}) => {
  return (
    <TouchableOpacity
      onPress={() => onPress(item)}
      activeOpacity={0.7}
      style={CountryModalStyles.listItemContainer}>
      <View style={CountryModalStyles.listItemContent}>
        <View style={CountryModalStyles.iconContainer}>
          {selected === item.country && (
            <Tick style={CountryModalStyles.tickIcon} />
          )}
        </View>
        <Text style={CountryModalStyles.listItemText}>{item.country}</Text>
      </View>
    </TouchableOpacity>
  );
};

export const CountryModal: FC<CountryModalProps> = ({show = false}) => {
  const snapPoints = useMemo(() => ['70%', '80%', '90%', '100%'], []);
  const [selected, setSelected] = useState('');

  const selectAction = (item: CountryData) => {
    setSelected(prevSelected =>
      prevSelected === item.country ? '' : item.country,
    );
  };

  return show ? (
    <IModal
      handleIndicatorStyle={CountryModalStyles.handleIndicator}
      containerStyle={CountryModalStyles.container}
      backgroundStyle={CountryModalStyles.background}
      snapPoints={snapPoints}>
      <Input
        placeholder={intl('beforeAuth.countryModal.searchCountry')}
        style={CountryModalStyles.input}
        animationEnabled={false}
      />

      <BottomSheetFlatList
        style={CountryModalStyles.bottomSheetList}
        showsVerticalScrollIndicator={false}
        data={countryData}
        keyExtractor={(item, index) => ` ${index} ${item.id} `}
        renderItem={({item}) => (
          <ListItem item={item} selected={selected} onPress={selectAction} />
        )}
      />
    </IModal>
  ) : (
    <></>
  );
};
