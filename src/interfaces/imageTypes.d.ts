declare module '*.png' {
  import type {ImageSourcePropType} from 'react-native';
  const imageSource: ImageSourcePropType;
  export {imageSource as default};
}

declare module '*.jpg' {
  import type {ImageSourcePropType} from 'react-native';
  const imageSource: ImageSourcePropType;
  export {imageSource as default};
}

declare module '*.jpeg' {
  import type {ImageSourcePropType} from 'react-native';
  const imageSource: ImageSourcePropType;
  export {imageSource as default};
}
