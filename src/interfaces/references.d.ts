import {NavigationContainerRef} from '@react-navigation/native';
import Loader from 'components/global/loader/loader';

type References = {
  navigator: NavigationContainerRef<any> | null;
  loader: Loader | null;
};
