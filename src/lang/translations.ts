export const en = {
  beforeAuth: {
    login: {
      login: 'Login',
      forgotPassword: 'Forgot Password?',
      email: 'Email',
      password: 'Password',
      register: "Don't have an account, Register?",
      logout: 'Logout',
    },

    register: {
      requiredFullName: 'Full name is a required field',
      minFullName: 'The field must be minimum 3 characters',
      fullNamePlaceholder: 'Full name',

      requiredTelNumber: 'Phone number is a required field',
      minTelNumber: 'Phone number must be minimum 11 characters',
      telNumberPlaceholder: 'Phone number',

      requiredPassword: 'Password is a required field',
      minPassword: 'Password must be minimum 10 characters',
      passwordPlaceholder: 'Password',

      requiredUserName: 'Username is a required field',
      minUserName: 'The field must be minimum 10 characters',
      userNamePlaceholder: 'Username',

      requiredEmail: 'Email is a required field',
      minEmail: 'Email must be minimum 11 characters',
      emailPlaceholder: 'Email',

      chooseCountryButtonTitle: 'Choose Country',
      registerTitle: 'Register',
    },

    validations: {
      minEmail: 'Email must be minimum 11 characters',
      requriedEmail: 'Email is required',

      minPassword: 'Password must be minimum 10 characters',
      requiredPassword: 'Password is required',
    },

    otp: {
      login: 'Login',
      enterOTP: 'Enter code from the SMS',
      changeNumber: 'Change Number',
      resend: 'Resend: ',
    },

    countryModal: {
      searchCountry: 'Search Country',
    },
  },
};

export const ge = {
  beforeAuth: {
    login: {
      login: 'შესვლა',
      forgotPassword: 'დაგავიწყდა პაროლი??',
      email: 'მეილი',
      password: 'პაროლი',
      register: 'არ გაქვს ანგარიში, დარეგისტრირდი',
      logout: 'გამოსვლა',
    },

    register: {
      requiredFullName: 'სახელი და გვარი აუცილებელი ველია',
      minFullName: 'ველი უნდა შეიცავდეს მინიმუმ 3 სიმბოლოს',
      fullNamePlaceholder: 'სახელი და გვარი',

      requiredTelNumber: 'ტელეფონის ნომერი აუცილებელი ველია',
      minTelNumber: 'ტელეფონის ნომერი უნდა შედგებოდეს მინიმუმ 11 სიმბოლოსგან',
      telNumberPlaceholder: 'ტელეფონის ნომერი',

      requiredPassword: 'პაროლი აუცილებელი ველია',
      minPassword: 'პაროლი უნდა შედგებოდეს მინიმუმ 10 სიმბოლოსგან',
      passwordPlaceholder: 'პაროლი',

      requiredUserName: 'მომხმარებლის სახელი აუცილებელი ველია',
      minUserName: 'ველი უნდა შედგებოდეს მინიმუმ 10 სიმბოლოსგან',
      userNamePlaceholder: 'მომხმარებლის სახელი',

      requiredEmail: 'მეილი აუცილებელი ველია',
      minEmail: 'მეილი უნდა შედგებოდეს მინიმუმ 11 სიმბოლოსგან',
      emailPlaceholder: 'მეილი',

      chooseCountryButtonTitle: 'აირჩიეთ ქვეყანა',
      registerTitle: 'რეგისტრაცია',
    },

    validations: {
      minEmail: 'მეილი უნდა შედგებოდეს მინიმუმ 11 სიმბოლოსგან',
      requriedEmail: 'მეილი აუცილებელი ველია',

      minPassword: 'პაროლი უნდა შედგებოდეს მინიმუმ 10 სიმბოლოსგან',
      requiredPassword: 'პაროლი აუცილებელი ველია',
    },

    otp: {
      login: 'შესვლა',
      enterOTP: 'შეიყვანეთ SMS-ით მიღებული კოდი',
      changeNumber: 'ნომრის შეცვლა',
      resend: 'თავიდან გაგზავნა: ',
    },
    countryModal: {
      searchCountry: 'მოძებნე ქვეყანა',
    },
  },
};
