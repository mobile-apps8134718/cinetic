import AsyncStorage from '@react-native-community/async-storage';
import {Platform, NativeModules} from 'react-native';
import {initReactI18next} from 'react-i18next';
import {en, ge} from './translations';
import {cloneDeep} from 'lodash';
import i18n from 'i18next';
import 'intl-pluralrules';

const englishDeepCloneResource = cloneDeep(en);
const georgianDeepCloneResource = cloneDeep(ge);

const getLocale = (localestring: string): string => {
  return localestring.split('_')[0];
};

const languageDetector: any = {
  type: 'languageDetector',
  async: true,
  detect: (callback: any) => {
    let locale: string = 'en';

    AsyncStorage.getItem('lng', (err, lng) => {
      if (err || !lng) {
        if (err && __DEV__) {
          console.log('Error fetching Languages from AsyncStorage', err);
        } else if (__DEV__) {
          console.log('No language is set, choosing English as fallback');
        }
        if (Platform.OS === 'ios') {
          locale = getLocale(
            NativeModules.SettingsManager.settings.AppleLanguages[0],
          );
        } else if (Platform.OS === 'android') {
          locale = getLocale(NativeModules.I18nManager.localeIdentifier);
        }
        callback(locale.replace('_', '-'));
        return;
      }
      callback(lng);
    });
  },
  init: () => {},
  cacheUserLanguage: (lng: string) => {
    let lang = lng.split('-')[0];
    AsyncStorage.setItem('lng', lang);
  },
};

const i18 = i18n
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    debug: false,
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
    cache: {
      enabled: true,
    },

    react: {
      useSuspense: false,
    },

    load: 'currentOnly',

    resources: {
      en: {
        translation: {
          ...englishDeepCloneResource,
        },
      },

      ge: {
        translation: {
          ...georgianDeepCloneResource,
        },
      },
    },
  });

export const initTranslation = () => {
  return i18;
};
