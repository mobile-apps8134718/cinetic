import {Button} from 'components/shared/button/button';
import {navigate} from 'navigation/navigationActions';
import {Input} from 'components/shared/input/input';
import {Controller, useForm} from 'react-hook-form';
import {LoginFormStyles} from './formStyles';
import {View} from 'react-native';
import React, {FC} from 'react';
import {intl} from 'hooks/intl';

interface Props {}

interface LoginFields {
  email: string;
  password: string;
}

export const LoginForm: FC<Props> = () => {
  const {
    reset,
    control,
    handleSubmit,
    formState: {errors},
  } = useForm<LoginFields>({
    defaultValues: {
      email: 'darsalia.david1998@gmail.com',
      password: 'sx149da2012mg49va91234',
    },
  });

  const placeholders = [
    intl('beforeAuth.login.email'),
    intl('beforeAuth.login.password'),
  ];

  const onSubmit = () => {
    reset({
      email: '',
      password: '',
    });
    navigate('OTP');
  };

  const navigateToRegister = () => {
    navigate('Register');
  };

  return (
    <View style={LoginFormStyles.wrapper}>
      <Controller
        control={control}
        rules={{
          required: {
            value: true,
            message: intl('beforeAuth.validations.requriedEmail'),
          },
          minLength: {
            value: 11,
            message: intl('beforeAuth.validations.minEmail'),
          },
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <Input
            testID={'EmailInput'}
            error={`${errors?.email?.message || ''}`}
            placeholder={placeholders[0]}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
            autoFocus
          />
        )}
        name="email"
      />

      <Controller
        control={control}
        rules={{
          required: {
            value: true,
            message: intl('beforeAuth.validations.requiredPassword'),
          },
          minLength: {
            value: 10,
            message: intl('beforeAuth.validations.minPassword'),
          },
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <Input
            testID={'PasswordInput'}
            error={`${errors.password?.message || ''}`}
            placeholder={placeholders[1]}
            onChangeText={onChange}
            secureTextEntry
            onBlur={onBlur}
            value={value}
          />
        )}
        name="password"
      />

      <Button
        testID={'ForgotPasswordButton'}
        textStyles={LoginFormStyles.textButtonStyle}
        buttonStyles={LoginFormStyles.buttonStyle}
        variant="Text"
        title={intl('beforeAuth.login.forgotPassword')}
        throttleTime={0}
        onPress={() => {}}
      />

      <Button
        testID={'LoginButton'}
        variant="Filled"
        throttleTime={0}
        title={intl('beforeAuth.login.login')}
        onPress={handleSubmit(onSubmit)}
      />

      <Button
        testID={'NavigateToRegisterButton'}
        textStyles={LoginFormStyles.registerButton}
        buttonStyles={LoginFormStyles.buttonStyle}
        variant="Text"
        title={intl('beforeAuth.login.register')}
        throttleTime={0}
        onPress={navigateToRegister}
      />
    </View>
  );
};
