import {StyleSheet} from 'react-native';
import {height} from '../constants/dimensions';
import colors from '../constants/colors';

export const LoginFormStyles = StyleSheet.create({
  wrapper: {
    width: '95%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  buttonStyle: {
    alignSelf: 'flex-end',
    marginRight: '3%',
    marginVertical: '1%',
  },
  textButtonStyle: {
    fontSize: 13,
    fontWeight: '400',
  },
  registerButton: {
    alignSelf: 'center',
    marginTop: '5%',
    fontWeight: '400',
  },
});

export const OTPFormStyles = StyleSheet.create({
  wrapper: {
    width: '95%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    position: 'relative',
  },
  inputWrapper: {
    width: '1%',
    height: height * 0.1,
    position: 'absolute',
    borderWidth: 0,
    fontSize: 0,
    right: '-4%',
  },
  infoContainer: {
    width: '95%',
    height: '15%',
    marginBottom: '10%',
    paddingVertical: '2%',
    paddingHorizontal: '2%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  infoTitle: {
    color: 'white',
    fontWeight: '700',
    fontSize: 25,
  },
  infoText: {
    color: colors.fontColor,
  },
  digitContainer: {
    width: '90%',
    height: '10%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: '5%',
  },
  digit: {
    width: '22%',
    height: '100%',
    borderWidth: 0.5,
    borderColor: colors.fontColor,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  digitText: {
    fontWeight: '500',
    color: colors.white,
    fontSize: 25,
  },
  submitBtnStyle: {
    width: '95%',
    marginBottom: '-3%',
  },
  buttonWrapper: {
    width: '90%',
    height: '9%',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginVertical: '5%',
  },
});

export const RegisterFormStyles = StyleSheet.create({
  wrapper: {
    width: '95%',
  },
  countryButton: {
    marginVertical: '3%',
    alignSelf: 'flex-end',
    marginRight: '4%',
  },
  backButton: {
    top: '-30%',
    left: '1%',
    position: 'absolute',
  },
});
