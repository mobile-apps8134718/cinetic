import {fireEvent, render} from '@testing-library/react-native';
import {LoginForm} from './loginForm';
import {intl} from '../hooks/intl';
import React from 'react';

describe('Login Form', () => {
  it('Testing form render on correctness', () => {
    const {getByTestId} = render(<LoginForm />);

    // Inputs Render
    expect(getByTestId('EmailInput')).toBeTruthy();
    expect(getByTestId('PasswordInput')).toBeTruthy();

    // Buttons Render
    expect(getByTestId('ForgotPasswordButton')).toBeTruthy();
    expect(getByTestId('LoginButton')).toBeTruthy();
    expect(getByTestId('NavigateToRegisterButton')).toBeTruthy();
  });

  it('Testing the form inputs changing abilities', () => {
    const {getByTestId} = render(<LoginForm />);

    const emailInput = getByTestId('EmailInput');
    const passwordInput = getByTestId('PasswordInput');

    // Checking changeability of inputs
    fireEvent.changeText(emailInput, 'darsalia@gmail.com');
    fireEvent.changeText(passwordInput, 'password123');
  });

  it('Checking placeholder values and making sure, they are translated', () => {
    const {getByTestId} = render(<LoginForm />);

    const emailInput = getByTestId('EmailInput');
    const passwordInput = getByTestId('PasswordInput');

    const submitButton = getByTestId('LoginButton');
    const registerButton = getByTestId('NavigateToRegisterButton');
    const forgotPasswordButton = getByTestId('ForgotPasswordButton');

    expect(emailInput.props.placeholder).toBe(intl('beforeAuth.login.email'));
    expect(passwordInput.props.placeholder).toBe(
      intl('beforeAuth.login.password'),
    );

    expect(submitButton.props.title).toBe(intl('beforeAuth.login.login'));
    expect(forgotPasswordButton.props.title).toBe(
      intl('beforeAuth.login.forgotPassword'),
    );
    expect(registerButton.props.title).toBe(intl('beforeAuth.login.register'));
  });

  it('Testing form submission', () => {});
});
