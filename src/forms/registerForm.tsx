import {CountryModal} from '../components/shared/countryModal/countryModal';
import {SVGIconWrapper} from '../components/shared/svgIconWrapper';
import {Button} from '../components/shared/button/button';
import {Input} from '../components/shared/input/input';
import {goBack} from '../navigation/navigationActions';
import BackIcon from 'assets/icons/cross/backIcon.svg';
import {Controller, useForm} from 'react-hook-form';
import {RegisterFormStyles} from './formStyles';
import {intl} from '../hooks/intl';
import {View} from 'react-native';
import React, {FC} from 'react';
import {replace} from '../navigation/methods/navigationActions';

interface Registerfields {
  fullName: string;
  telNumber: string;
  password: string;
  userName: string;
  email: string;
}

export const RegisterForm: FC = ({}) => {
  const {
    reset,
    control,
    handleSubmit,
    formState: {errors},
  } = useForm<Registerfields>({
    defaultValues: {
      fullName: 'Davit Darsalia',
      // Todo - apply format for number
      telNumber: '+995 598 29 92 89',
      password: 'SomePasswordSha256',
      userName: 'David.Darsalia',
      email: 'darsalia.david1998@gmail.com',
    },
  });

  const placeHolders = [
    intl('beforeAuth.register.fullNamePlaceholder'),
    intl('beforeAuth.register.telNumberPlaceholder'),
    intl('beforeAuth.register.passwordPlaceholder'),
    intl('beforeAuth.register.userNamePlaceholder'),
    intl('beforeAuth.register.emailPlaceholder'),
  ];

  const onSubmit = () => {
    reset({
      fullName: '',
      telNumber: '',
      password: '',
      userName: '',
      email: '',
    });
    replace('Login');
  };

  const BackAction = () => {
    goBack();
  };

  return (
    <View style={RegisterFormStyles.wrapper}>
      <SVGIconWrapper
        onPress={BackAction}
        style={RegisterFormStyles.backButton}>
        <BackIcon />
      </SVGIconWrapper>

      <Controller
        control={control}
        rules={{
          required: {
            value: true,
            message: intl('beforeAuth.register.requiredFullName'),
          },
          minLength: {
            value: 3,
            message: intl('beforeAuth.register.minFullName'),
          },
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <Input
            error={`${errors.fullName?.message || ''}`}
            placeholder={placeHolders[0]}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
            autoFocus
          />
        )}
        name={'fullName'}
      />

      <Controller
        control={control}
        rules={{
          required: {
            value: true,
            message: intl('beforeAuth.register.requiredTelNumber'),
          },
          minLength: {
            value: 11,
            message: intl('beforeAuth.register.minTelNumber'),
          },
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <Input
            error={`${errors.telNumber?.message || ''}`}
            placeholder={placeHolders[1]}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
          />
        )}
        name={'telNumber'}
      />

      <Controller
        control={control}
        rules={{
          required: {
            value: true,
            message: intl('beforeAuth.register.requiredPassword'),
          },
          minLength: {
            value: 10,
            message: intl('beforeAuth.register.minPassword'),
          },
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <Input
            error={`${errors.password?.message || ''}`}
            placeholder={placeHolders[2]}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
          />
        )}
        name={'password'}
      />

      <Controller
        control={control}
        rules={{
          required: {
            value: true,
            message: intl('beforeAuth.register.requiredUserName'),
          },
          minLength: {
            value: 10,
            message: intl('beforeAuth.register.minUserName'),
          },
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <Input
            error={`${errors.userName?.message || ''}`}
            placeholder={placeHolders[3]}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
          />
        )}
        name={'userName'}
      />

      <Controller
        control={control}
        rules={{
          required: {
            value: true,
            message: intl('beforeAuth.register.requiredEmail'),
          },
          minLength: {
            value: 10,
            message: intl('beforeAuth.register.requiredEmail'),
          },
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <Input
            error={`${errors.email?.message || ''}`}
            placeholder={placeHolders[4]}
            onChangeText={onChange}
            onBlur={onBlur}
            value={value}
          />
        )}
        name={'email'}
      />

      <Button
        testID={'ChooseCountryButton'}
        title={intl('beforeAuth.register.chooseCountryButtonTitle')}
        buttonStyles={RegisterFormStyles.countryButton}
        onPress={() => {}}
        throttleTime={0}
        variant={'Text'}
      />

      <CountryModal show={false} />

      <Button
        testID={'RegisterButton'}
        title={intl('beforeAuth.register.registerTitle')}
        onPress={handleSubmit(onSubmit)}
        variant={'Filled'}
      />
    </View>
  );
};
