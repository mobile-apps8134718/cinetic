import {goBack, goTo} from 'navigation/navigationActions';
import {Button} from 'components/shared/button/button';
import {Text, TextInput, View} from 'react-native';
import {Controller, useForm} from 'react-hook-form';
import {Timer} from 'components/shared/timer/timer';
import {authStorage} from 'storage/authStorage';
import {OTPFormStyles} from './formStyles';
import {intl} from 'hooks/intl';
import React, {FC} from 'react';

interface Props {}

interface OTPField {
  OTP: string;
}

export const OTPForm: FC<Props> = () => {
  const {control, handleSubmit, reset} = useForm<OTPField>({
    defaultValues: {
      OTP: '3512',
    },
  });

  const resetForm = () => {
    reset({
      OTP: '',
    });
  };

  const onSubmit = (data: OTPField) => {
    if (data.OTP.length === 4) {
      try {
        authStorage.set('SessionIdentifier', 'dsfsdfaf.2131rt1grs.fg134ge');
      } finally {
        goTo('Tabs', 'Home');
      }
    }
  };

  const onChangeNumberPress = () => {
    resetForm();
    goBack();
  };

  const onResend = () => {
    resetForm();
  };

  return (
    <>
      <Controller
        control={control}
        rules={{
          minLength: {
            value: 4,
            message: intl('beforeAuth.validations.minEmail'),
          },
        }}
        render={({field: {onChange, onBlur, value}}) => (
          <>
            <TextInput
              onChangeText={onChange}
              onBlur={onBlur}
              value={value}
              keyboardType="number-pad"
              autoFocus
              maxLength={4}
              style={OTPFormStyles.inputWrapper}
            />

            <View style={OTPFormStyles.infoContainer}>
              <Text style={OTPFormStyles.infoTitle}>
                {intl('beforeAuth.otp.login')}
              </Text>
              <Text style={OTPFormStyles.infoText}>
                Enter the password from the SMS
              </Text>
            </View>

            <View style={OTPFormStyles.digitContainer}>
              {[1, 2, 3, 4].map((cell, index) => (
                <View key={index} style={OTPFormStyles.digit}>
                  <Text style={OTPFormStyles.digitText}>{value[index]}</Text>
                </View>
              ))}
            </View>

            <View style={OTPFormStyles.buttonWrapper}>
              <Button
                testID={'ChangeNumberButton'}
                buttonStyles={{}}
                variant="Text"
                throttleTime={0}
                title={intl('beforeAuth.otp.changeNumber')}
                onPress={onChangeNumberPress}
              />

              <Timer
                label={intl('beforeAuth.otp.resend')}
                onPress={onResend}
                count={60}
              />
            </View>

            <Button
              testID={'OTPSubmitButton'}
              buttonStyles={OTPFormStyles.submitBtnStyle}
              variant="Filled"
              throttleTime={0}
              title={intl('beforeAuth.login.login')}
              onPress={handleSubmit(onSubmit)}
            />
          </>
        )}
        name="OTP"
      />
    </>
  );
};
