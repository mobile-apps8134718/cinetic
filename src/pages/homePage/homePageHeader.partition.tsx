import React from 'react';
import {Text, View} from 'react-native';
import MainLogo from 'assets/icons/cross/MainLogo.svg';
import Location from 'assets/icons/cross/Location.svg';
import Language from 'assets/icons/cross/Language.svg';
import {getCurrentLanguage} from 'hooks/getCurrentLanguage';
import {Button} from 'components/shared/button/button';
import {intl} from 'hooks/intl';
import {homePageStyles} from '../pageStyles';
import {replace} from '../../navigation/methods/navigationActions';
import {deleteValueFromSecureStorage} from '../../hooks/secureStorage';

export const HomePageHeader = () => {
  const onPressLogin = () => {
    deleteValueFromSecureStorage('SessionIdentifier');
    replace('BeforeAuth');
  };

  return (
    <View style={homePageStyles.container}>
      <MainLogo style={homePageStyles.logo} />

      <View style={homePageStyles.menuContainer}>
        <View style={homePageStyles.menuItem}>
          <Location />
          <Text style={homePageStyles.menuItemText}>London</Text>
        </View>

        <View style={homePageStyles.menuItem}>
          <Language />
          <Text style={homePageStyles.menuItemText}>
            {getCurrentLanguage()?.toUpperCase() || 'EN'}
          </Text>
        </View>
      </View>

      <Button
        buttonStyles={homePageStyles.button}
        textStyles={homePageStyles.buttonText}
        testID={'LoginButton'}
        title={intl('beforeAuth.login.logout')}
        onPress={onPressLogin}
        variant={'Filled'}
      />
    </View>
  );
};
