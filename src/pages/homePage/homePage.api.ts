import {Movie} from './interfaces';
import axios from 'axios';

export const FetchMoviesData = async () => {
  const options = {
    method: 'GET',
    url: 'http://localhost:8080/movie-recommendations-list?limit=3000',
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const response = await axios.request<Movie[]>(options);
  return response.data;
};
