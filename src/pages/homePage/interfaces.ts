export interface Details {
  imdbRating: number;
  averageRating: number;
  movieDescription: string;
  minAge: number;
  runTime: string;
  releaseYear: number;
  genres: string[];
  directorName: string;
  cast: string[];
}

export interface Movie {
  movieID: string;
  imageURL: string;
  movieTitle: string;
  genre: string;
  rating: number;
  movieDetails: Details;
}
