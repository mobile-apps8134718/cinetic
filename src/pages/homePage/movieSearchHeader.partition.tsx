import React from 'react';
import {Text, View} from 'react-native';
import SearchIcon from '../../assets/icons/cross/searchIcon.svg';
import {homePageStyles} from '../pageStyles';

export const MovieSearchHeader = () => {
  return (
    <View style={homePageStyles.movieSearchHeaderWrapper}>
      <Text style={homePageStyles.NowTitle}>Now In Cinemas</Text>
      <SearchIcon />
    </View>
  );
};
