import React, {FC} from 'react';
import {MovieRenderItem} from './movie.renderItem';
import {FlashList} from '@shopify/flash-list';
import {useQuery} from '@tanstack/react-query';
import {Movie} from './interfaces';
import {FetchMoviesData} from './homePage.api';
import {NativeScrollEvent, NativeSyntheticEvent} from 'react-native';

interface Props {
  scrollFunc: (event: NativeSyntheticEvent<NativeScrollEvent>) => void;
}
export const MovieList: FC<Props> = ({scrollFunc}) => {
  const {data, isLoading} = useQuery<Movie[]>(['moviesAPI'], FetchMoviesData);

  return (
    <>
      {data && !isLoading && (
        <FlashList
          showsVerticalScrollIndicator={false}
          onScroll={scrollFunc}
          numColumns={2}
          data={data || []}
          renderItem={({item}) => (
            <MovieRenderItem
              imageURL={item.imageURL}
              movieTitle={item.movieTitle}
              genre={item.genre}
              rating={item.rating}
            />
          )}
          estimatedItemSize={293}
          keyExtractor={item => item.movieID}
        />
      )}
    </>
  );
};
