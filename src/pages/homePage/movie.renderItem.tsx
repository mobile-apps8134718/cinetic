import React, {FC} from 'react';
import {ImageBackground, Text, TouchableOpacity, View} from 'react-native';
import {Button} from '../../components/shared/button/button';
import {Movie} from './interfaces';
import {homePageStyles} from '../pageStyles';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';

export const MovieRenderItem: FC<Partial<Movie>> = ({
  imageURL = '',
  movieTitle = '',
  genre = '',
  rating = '',
}) => {
  const scale = useSharedValue(1);

  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          scale: withTiming(scale.value, {
            duration: 500,
          }),
        },
      ],
    };
  });

  const onPressIn = () => {
    scale.value = 0.9;
  };

  const onPressOut = () => {
    scale.value = 1;
  };

  return (
    <Animated.View
      style={[animatedStyle, homePageStyles.movieItemAnimatedWrapper]}>
      <TouchableOpacity
        onPressIn={onPressIn}
        onPressOut={onPressOut}
        activeOpacity={0.7}
        style={homePageStyles.movieItemWrapper}>
        {/*Image that wraps the entire component */}
        <ImageBackground
          style={homePageStyles.movieItemImage}
          source={{
            uri: imageURL,
          }}
        />

        {/* Movie Details */}
        <View style={homePageStyles.movieItemDetails}>
          <Text
            lineBreakMode={'middle'}
            style={homePageStyles.movieItemTitle}
            numberOfLines={1}>
            {movieTitle}
          </Text>
          <Text style={homePageStyles.movieItemGenre} numberOfLines={1}>
            {genre}
          </Text>
        </View>

        {/* Logout button */}
        <Button
          buttonStyles={homePageStyles.movieItemRatingButton}
          textStyles={homePageStyles.movieItemRatingText}
          disabled={true}
          title={`${rating}`.toString().slice(0, 3)}
          testID={'RatingDisabledButton'}
          variant={'Filled'}
        />
      </TouchableOpacity>
    </Animated.View>
  );
};
