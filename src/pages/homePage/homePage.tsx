import {
  NativeScrollEvent,
  NativeSyntheticEvent,
  SafeAreaView,
} from 'react-native';
import {HomePageHeader} from './homePageHeader.partition';
import React from 'react';
import {MovieSearchHeader} from './movieSearchHeader.partition';
import {homePageStyles} from '../pageStyles';
import Animated, {
  useAnimatedStyle,
  useSharedValue,
  interpolate,
  Extrapolate,
  withTiming,
} from 'react-native-reanimated';
import {MovieList} from './movieList.partition';

// Optimize list
export const HomePage = () => {
  const translateY = useSharedValue(0);
  const translateYList = useSharedValue(0);
  const opacity = useSharedValue(1);

  const animatedStyle = useAnimatedStyle(() => {
    return {
      opacity: interpolate(
        translateY.value,
        [0, -200],
        [1, 0],
        Extrapolate.CLAMP,
      ),
      transform: [{translateY: translateY.value}],
    };
  });

  const animatedListStyle = useAnimatedStyle(() => {
    return {
      transform: [{translateY: translateYList.value}],
    };
  });

  const handleScroll = (() => {
    let previousOffset = 100;

    return (event: NativeSyntheticEvent<NativeScrollEvent>) => {
      if (
        event.nativeEvent.contentOffset.y > previousOffset &&
        event.nativeEvent.contentOffset.y > 50
      ) {
        opacity.value = 0;
        translateY.value = withTiming(-200, {duration: 1000});
        translateYList.value = withTiming(-80, {duration: 1000});
      } else if (event.nativeEvent.contentOffset.y < previousOffset) {
        opacity.value = 1;
        translateY.value = withTiming(0, {duration: 500});
        translateYList.value = withTiming(0, {duration: 500});
      }
      previousOffset = event.nativeEvent.contentOffset.y;
    };
  })();

  return (
    <SafeAreaView style={homePageStyles.homePageWrapper}>
      <Animated.View
        style={[animatedStyle, homePageStyles.animatedHeaderContainer]}>
        <HomePageHeader />
        <MovieSearchHeader />
      </Animated.View>

      <Animated.View
        style={[animatedListStyle, homePageStyles.homePageInnerWrapper]}>
        <MovieList scrollFunc={handleScroll} />
      </Animated.View>
    </SafeAreaView>
  );
};
