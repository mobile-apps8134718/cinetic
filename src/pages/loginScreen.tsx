import {InputWrapper} from 'components/shared/inputWrapper/inputWrapper';
import {SafeAreaView, StyleSheet} from 'react-native';
import {width, height} from 'constants/dimensions';
import {LoginForm} from 'forms/loginForm';
import colors from 'constants/colors';
import React from 'react';

export const LoginScreen = () => {
  return (
    <SafeAreaView style={styles.center}>
      <InputWrapper>
        <LoginForm />
      </InputWrapper>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  center: {
    width,
    height,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.backgroundPurple,
  },
});
