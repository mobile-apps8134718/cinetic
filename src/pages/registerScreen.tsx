import {SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';
import {InputWrapper} from '../components/shared/inputWrapper/inputWrapper';
import {RegisterForm} from '../forms/registerForm';
import {height, width} from '../constants/dimensions';
import colors from '../constants/colors';

export const RegisterScreen = () => {
  return (
    <SafeAreaView style={styles.center}>
      <InputWrapper>
        <RegisterForm />
      </InputWrapper>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  center: {
    width,
    height,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.backgroundPurple,
  },
});
