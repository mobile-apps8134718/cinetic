import {Button, StyleSheet, View} from 'react-native';
import {goTo} from 'navigation/navigationActions';
import {authStorage} from 'storage/authStorage';
import {Text} from 'react-native';
import React from 'react';

export const First = () => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.textStyle}>
        {authStorage.getString('SessionIdentifier')}
      </Text>

      <Button
        title={'DEL'}
        onPress={() => {
          goTo('BeforeAuth', 'Login');
          authStorage.delete('SessionIdentifier');
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: 'white',
  },
  textStyle: {
    marginTop: 300,
  },
});
