import {InputWrapper} from 'components/shared/inputWrapper/inputWrapper';
import {StyleSheet, SafeAreaView} from 'react-native';
import {width, height} from 'constants/dimensions';
import colors from 'constants/colors';
import {OTPForm} from 'forms/otpForm';
import React, {FC} from 'react';

interface Props {}

export const OTPScreen: FC<Props> = () => {
  return (
    <SafeAreaView style={styles.center}>
      <InputWrapper>
        <OTPForm />
      </InputWrapper>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  center: {
    width,
    height,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: colors.backgroundPurple,
  },
});
