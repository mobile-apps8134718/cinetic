import {create} from 'zustand';

export interface User {
  id: number;
  name: string;
  email: string;
  password: string;
  createdAt: Date;
}

type Store = {
  users: User[];
  addUser: (user: User) => void;
};

const initialState: User[] = [
  {
    id: 1,
    name: 'John Doe',
    email: 'JohnDoe@gmail.com',
    password: '12345',
    createdAt: new Date(),
  },
  {
    id: 2,
    name: 'Oleg mongol',
    email: 'OlegMongol@gmail.com',
    password: 'olegmongol',
    createdAt: new Date(),
  },
];

export const mainStore = create<Store>(set => ({
  users: initialState,
  addUser(user: User) {
    set(state => ({
      ...state,
      users: [...state.users, user],
    }));
  },
}));
