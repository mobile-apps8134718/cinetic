import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {LoginScreen} from 'pages/loginScreen';
import {RegisterScreen} from 'pages/registerScreen';
import {OTPScreen} from 'pages/otpScreen';
import React from 'react';

export type BeforeAuthScreenList = {
  OTP: undefined;
  Login: undefined;
  Register: undefined;
};

const Stack = createNativeStackNavigator<BeforeAuthScreenList>();

export const BeforeAuth: React.FC = () => {
  const {Navigator, Screen} = Stack;
  return (
    <Navigator screenOptions={{headerShown: false}}>
      <Screen name="Login" component={LoginScreen} />
      <Screen name="OTP" component={OTPScreen} />
      <Screen name="Register" component={RegisterScreen} />
    </Navigator>
  );
};
