import {references} from 'components/componentReferences';
import {CommonActions} from '@react-navigation/native';

/**
 @description Use it rarely. Manual navigation through stacks
 */
export const goTo = (stack: string, screen: string): void => {
  references.navigator?.dispatch(CommonActions.navigate(stack, {screen}));
};

/**
 @description Regular navigation function, similar from useNavigation()
 */
export const navigate = (screen: string): void => {
  references.navigator?.dispatch(CommonActions.navigate(screen));
};

/**
 @description Go back action
 */
export const goBack = (): void => {
  references.navigator?.dispatch(CommonActions.goBack());
};
