import colors from '../constants/colors';

export const RootRouterScreenOptions = {
  headerShown: false,
  freezeOnBlur: true,
  fullScreenGestureEnabled: false,
  gestureEnabled: false,
};

export const TabBarScreenOptions = {
  tabBarActiveTintColor: 'tomato',
  tabBarInactiveTintColor: 'gray',
  headerShown: false,
  tabBarLabelStyle: {
    fontSize: 15,
  },
  tabBarItemStyle: {
    borderTopWidth: 1,
    borderTopColor: colors.fontColor,
    paddingTop: 5,
  },
};
