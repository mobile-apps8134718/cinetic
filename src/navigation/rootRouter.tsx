import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {saveGlobalNavigationObjectReference} from './methods/navigationActions';
import {NavigationContainer} from '@react-navigation/native';
import {useColorScheme} from 'react-native';
import {DarkTheme, LightTheme} from 'constants/theme';
import {authStorage} from '../storage/authStorage';
import {RootRouterScreenOptions} from './options';
import React from 'react';
import {BeforeAuth} from './beforeAuth';
import {BottomTabs} from './tabNavigator';

export type RootScreenList = {
  BeforeAuth: undefined;
  Tabs: undefined;
};

export const RootRouter = () => {
  const {Navigator, Screen} = createNativeStackNavigator<RootScreenList>();
  const theme = useColorScheme();

  return (
    <NavigationContainer
      theme={theme === 'light' ? LightTheme : DarkTheme}
      ref={saveGlobalNavigationObjectReference}>
      <Navigator
        initialRouteName={
          authStorage.getString('SessionIdentifier') ? 'Tabs' : 'BeforeAuth'
        }
        screenOptions={RootRouterScreenOptions}>
        <Screen name="BeforeAuth" component={BeforeAuth} />
        <Screen name="Tabs" component={BottomTabs} />
      </Navigator>
    </NavigationContainer>
  );
};
