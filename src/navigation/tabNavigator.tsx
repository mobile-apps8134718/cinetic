import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {HomePage} from '../pages/homePage/homePage';
import {TabBarScreenOptions} from './options';
import React, {lazy} from 'react';

interface IScreen {
  name: string;
  component: React.ComponentType<any>;
}

const screenData: Readonly<IScreen[]> = [
  {name: 'Home', component: HomePage},
  {
    name: 'Settings',
    component: lazy(() =>
      import('pages/second').then(module => ({default: module.Second})),
    ),
  },
  {
    name: 'First',
    component: lazy(() =>
      import('pages/first').then(module => ({default: module.First})),
    ),
  },
  {
    name: 'Second',
    component: lazy(() =>
      import('pages/second').then(module => ({default: module.Second})),
    ),
  },
] as const;

export const BottomTabs = () => {
  const {Navigator, Screen} = createBottomTabNavigator();

  return (
    <Navigator initialRouteName={'Home'} screenOptions={TabBarScreenOptions}>
      {screenData.map(({name, component}) => {
        return <Screen key={name} name={name} component={component} />;
      })}
    </Navigator>
  );
};
