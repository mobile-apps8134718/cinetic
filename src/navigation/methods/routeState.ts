import {references} from 'components/componentReferences';

/**
 @description Get current route
 */
export const getCurrentRoute = () => {
  return references.navigator?.getCurrentRoute()?.name;
};
