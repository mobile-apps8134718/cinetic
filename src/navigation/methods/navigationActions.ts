/**
 @ModuleDescription In this file all actions replaces navigation actions from useNavigation() hook
 */

import {
  NavigationContainerRef,
  CommonActions,
  StackActions,
} from '@react-navigation/native';
import {references} from 'components/componentReferences';

/**
 @description used only for initialization at NavigationContainer of RootRouter
 */
export const saveGlobalNavigationObjectReference = (
  navigator: NavigationContainerRef<any>,
): void => {
  references.navigator = navigator;
};

/**
 @description Use it rarely. Manual navigation
 @analogue const { goTo } = useNavigation()
 */
export const goTo = (stack: string, screen: string): void => {
  references.navigator?.dispatch(CommonActions.navigate(stack, {screen}));
};

/**
 @description Regualr navigation function, similar from useNavigation()
 @analogue const { navigate } = useNavigation()
 */
export const navigate = (screen: string): void => {
  references.navigator?.dispatch(CommonActions.navigate(screen));
};

/**
 @description Go back action
 @analogue const { goBack } = useNavigation()
 */
export const goBack = (): void => {
  references.navigator?.dispatch(CommonActions.goBack());
};

// Stack Actions

/**
 @description Replace action
 @analogue const { replace } = useNavigation()
 */
export const replace = (screen: string): void => {
  references.navigator?.dispatch(StackActions.replace(screen));
};

export const push = (screen: string): void => {
  references.navigator?.dispatch(StackActions.push(screen));
};
