/**
 @ModuleDescription In this file all actions replaces drawer actions.
 All metods are using Global Navigation Ref
 */

import {DrawerActions} from '@react-navigation/native';
import {references} from 'components/componentReferences';

/**
 @description Open drawer
 */
export const openDrawer = () => {
  references.navigator?.dispatch(DrawerActions.openDrawer());
};

/**
 @description Open drawer
 */
export const closeDrawer = () => {
  references.navigator?.dispatch(DrawerActions.closeDrawer());
};

/**
 @description Toggle driver state - Open or Close
 */
export const toggleDrawer = () => {
  references.navigator?.dispatch(DrawerActions.toggleDrawer());
};
