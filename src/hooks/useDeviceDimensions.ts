import {Dimensions} from 'react-native';

interface DeviceDimensions {
  width: number;
  height: number;
}

/**
 * Returns the dimensions of the device screen.
 * @returns {DeviceDimensions} Object containing the width and height of the device screen.
 * @example const { width, height } = useDeviceDimensions();
 */
export const useDeviceDimensions = (): DeviceDimensions => {
  return {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  };
};
