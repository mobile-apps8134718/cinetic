/**
 @ModuleDescription In this file all actions are defined for Secure Storage
 (Keychain, KeyStore) Use these functions to store, update or delete only sensitive info
 */

import {MMKV} from 'react-native-mmkv';

const mmkv = new MMKV();

type AsyncStorageKey = string;

interface SetSecureStorage {
  key: string;
  value: string;
}

export const getValueFromSecureStorage = async (
  key: AsyncStorageKey,
): Promise<string> => {
  return mmkv.getString(key) || 'No Key';
};

export const setValueOfSecureStorage = async (setObj: SetSecureStorage) => {
  mmkv.set(setObj.key, setObj.value);
};

export const deleteValueFromSecureStorage = async (
  key: AsyncStorageKey,
): Promise<void> => {
  mmkv.delete(key);
};

export const clearSecureStorage = async (): Promise<void> => {
  mmkv.clearAll();
};
