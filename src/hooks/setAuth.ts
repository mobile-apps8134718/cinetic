import {setValueOfSecureStorage} from './secureStorage';

type JWTIdentifier = `${string}.${string}.${string}`;

/*
    @Description - setAuth is responsible for setting JWT token at secure store. Provide a callback, which will be executed after setting value or default callback will be applied
*/
export const setAuth = (
  jwtIdentifier: JWTIdentifier,
  callback?: () => void,
): void => {
  const AUTH_SESSION = 'SessionIdentifier';

  const defaultCallback = () => {};

  setValueOfSecureStorage({
    key: AUTH_SESSION,
    value: jwtIdentifier,
  }).then(() => {
    (callback || defaultCallback)();
  });
};
