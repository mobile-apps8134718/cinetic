// import {LanguageList} from 'lang/i18NextInit';
import AsyncStorage from '@react-native-community/async-storage';
import i18n from 'i18next';

/**
 @param lang argument is only language, which is supported
 */
export const changeAppLanguage = (lang: 'ge' | 'en') => {
  AsyncStorage.getItem('lng').then(currentLanguage => {
    if (currentLanguage !== lang) {
      i18n.changeLanguage(lang);
    }
  });
};
