import i18n from 'i18next';

export const intl = (translationString: string) => {
  return i18n.t(translationString);
};
