/**
 @ModuleDescription In this file all actions are defined for Async Storage
 Use these functions to store, update or delete non-sensitive info
 */
import AsyncStorage from '@react-native-community/async-storage';

type AsyncStorageKey = string;

interface SetAsyncStorage {
  key: string;
  value: string;
}

/**
 @description Open drawer
 */
export const getValueFromAsyncStorage = (key: AsyncStorageKey): void => {
  AsyncStorage.getItem(key);
};

/**
 @description Open drawer
 */
export const setValueOfAsyncStorage = (setObj: SetAsyncStorage): void => {
  AsyncStorage.setItem(setObj.key, setObj.value);
};

/**
 @description Open drawer
 */
export const deleteValueFromAsyncStorage = (key: AsyncStorageKey): void => {
  AsyncStorage.removeItem(key);
};

/**
 @description Open drawer
 */
export const clearAsyncStorage = (): void => {
  AsyncStorage.clear;
};
