module.exports = {
  presets: [
    'module:metro-react-native-babel-preset',
    '@babel/preset-typescript',
  ],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./'],
        alias: {
          app: './src/app',
          components: './src/components',
          constants: './src/constants',
          helpers: './src/helpers',
          hooks: './src/hooks',
          interfaces: './src/interfaces',
          navigation: './src/navigation',
          pages: './src/pages',
          utils: './src/utils',
          lang: './src/lang',
          assets: './src/assets',
          forms: './src/forms',
          storage: './src/storage',
        },
      },
    ],
    'react-native-reanimated/plugin',
  ],
};
