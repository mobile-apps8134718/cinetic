import {name as appName} from './app.json';
import {AppRegistry} from 'react-native';
import 'react-native-gesture-handler';
import {MainApp} from './src/app/main.app';
import React from 'react';

const Root = () => <MainApp />;

AppRegistry.registerComponent(appName, () => Root);
